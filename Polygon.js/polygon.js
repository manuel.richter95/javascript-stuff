class Polygon {
	constructor(vertices) {
		this.vertices = vertices;
	}

	getVertices(decimals) {
		return this.vertices.map((v) => this.roundCoordinate(v, decimals));
	}

	getLength() {
		return this.vertices.length;
	}

	getCenter(decimals) {
		var sumX = 0,
			sumY = 0;
		for (var i = 0; i < this.vertices.length; i++) {
			sumX += this.vertices[i][0];
			sumY += this.vertices[i][1];
		}
		return this.roundCoordinate([sumX / this.vertices.length, sumY / this.vertices.length], decimals);
	}

	roundCoordinate(coordinate, decimals = 2) {
		return [parseFloat(Math.floor(coordinate[0].toFixed(decimals))), parseFloat(Math.floor(coordinate[0].toFixed(decimals)))];
	}
}

class EquilPolygon extends Polygon {
	constructor(center, size, n) {
		// Calculate the internal angle of the polygon
		const angle = 180 - 360 / n;

		// Calculate the angle between each side of the polygon
		const sideAngle = 180 - angle;

		// Create an empty list to store the coordinates
		const vertices = [];

		// Calculate the coordinates of each vertex
		for (let i = 0; i < n; i++) {
			vertices.push([
				center[0] + size * Math.cos(((angle / 2 + i * sideAngle) * Math.PI) / 180),
				center[1] + size * Math.sin(((angle / 2 + i * sideAngle) * Math.PI) / 180),
			]);
		}
		super(vertices);
	}
}

class Trigon extends EquilPolygon {
	constructor(center, size = 1) {
		super(center, size, 3);
	}
}

class Tetragon extends EquilPolygon {
	constructor(center, size = 1) {
		super(center, size, 4);
	}
}

class Pentagon extends EquilPolygon {
	constructor(center, size = 1) {
		super(center, size, 5);
	}
}

class Hexagon extends EquilPolygon {
	constructor(center, size = 1) {
		super(center, size, 6);
	}
}
