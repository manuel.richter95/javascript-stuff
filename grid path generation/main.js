class Random {
	static int(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}
}

class GridTile {
	constructor(x, y) {
		this.x = x;
		this.y = y;
		this.isPath = false;
		this.isStart = false;
		this.isEnd = false;
		this.visisted = false;
	}

	getMarker() {
		return this.isStart ? '[s]' : this.isEnd ? '[e]' : this.isPath ? '[#]' : '[ ]';
	}

	isDirectNeighbor(gridTile) {
		const n1 = [this.x - 1, this.y];
		const n2 = [this.x, this.y - 1];
		const n3 = [this.x + 1, this.y];
		const n4 = [this.x1, this.y + 1];

		for (let neighbor of [n1, n2, n3, n4]) {
			if (gridTile.x == neighbor[0] && gridTile.y == neighbor[1]) {
				return true;
			}
		}
		return false;
	}

	isDiagonalNeighbor(gridTile) {
		const n1 = [this.x - 1, this.y - 1];
		const n2 = [this.x + 1, this.y - 1];
		const n3 = [this.x + 1, this.y + 1];
		const n4 = [this.x - 1, this.y + 1];

		for (let neighbor of [n1, n2, n3, n4]) {
			if (gridTile.x == neighbor[0] && gridTile.y == neighbor[1]) {
				return true;
			}
		}
		return false;
	}

	log() {
		console.log(`{ x: ${this.x}, y: ${this.y} }`);
	}
}

class Path {
	constructor(minLength, startTile, endTile) {
		this.tiles = [];
		this.startTile = startTile;
		this.endTile = endTile;
		this.minLength = minLength;
		console.log('Path Length: ', this.minLength);
		this.generate();
	}

	generate() {
		const finished = false;
		while (this.tiles.length > this.minLength || !finished) {
			const side = Random.int(0, 3);
			let neighbor = null;
			switch (side) {
				case 0:
					//left
					point.x = 0;
					point.y = Random.int(1, this.lengthY - 1);
					break;
				case 1:
					//top
					point.x = Random.int(1, this.lengthX - 1);
					point.y = this.lengthX;
					break;
				case 2:
					//right
					point.x = this.lengthY;
					point.y = Random.int(1, this.lengthY - 1);
					break;
				case 3:
					//bottom
					point.x = Random.int(1, this.lengthY - 1);
					point.y = 0;
					break;
			}
		}
	}

	addTile(gridTile) {
		this.tiles.push(gridTile);
	}

	backtrace() {}
}

class Sector {
	constructor(lengthX, lengthY, startTile = null, endTile = null) {
		this.lengthX = lengthX;
		this.lengthY = lengthY;
		this.tiles = [];
		this.path = [];
		this.startTile = startTile;
		this.startSide = null;
		this.endTile = endTile;
		this.generate();
	}

	generate() {
		for (let i = 0; i <= this.lengthX; i++) {
			this.tiles[i] = new Array();
			for (let j = 0; j <= this.lengthX; j++) {
				this.tiles[i].push(new GridTile(i, j));
			}
		}
		if (!this.startTile) {
			this.startTile = this.generateStartEnd();
			this.startTile.isStart = true;
			this.startTile.isPath = true;
		}
		if (!this.endTile) {
			this.endTile = this.generateStartEnd();
			while (this.endTile.isDiagonalNeighbor(this.startTile)) {
				this.endTile = this.generateStartEnd();
			}
			this.endTile.isEnd = true;
		}
		this.path = [this.startTile];
		// for (let i = 0; i <= this.lengthX; i++) {
		// 	for (let j = 0; j <= this.lengthX; j++) {
		// 		this.tiles[i][j].log();
		// 	}
		// }
		this.createPath();
	}

	shuffle(a) {
		var j, x, i;
		for (i = a.length - 1; i > 0; i--) {
			j = Math.floor(Math.random() * (i + 1));
			x = a[i];
			a[i] = a[j];
			a[j] = x;
		}
		return a;
	}

	createPath() {
		this.startTile.log();
		this.endTile.log();
		const minPathLength = Random.int(28, 28);
		let finished = false;
		let i = 0;
		console.log(minPathLength);
		while (!finished) {
			const tile = this.path[i];
			let freePath = false;
			let neighbor = null;
			for (let direction of this.shuffle([0, 1, 2, 3])) {
				const side = Random.int(0, 3);
				switch (direction) {
					case 0:
						//left
						neighbor = this.tiles[tile.x - 1] ? this.tiles[tile.x - 1][tile.y] : null;
						break;
					case 1:
						//top
						neighbor = this.tiles[tile.x][tile.y - 1];
						break;
					case 2:
						//right
						neighbor = this.tiles[tile.x + 1] ? this.tiles[tile.x + 1][tile.y] : null;
						break;
					case 3:
						//bottom
						neighbor = this.tiles[tile.x][tile.y + 1];
						break;
				}
				if (neighbor) {
					if (neighbor.isPath) {
						freePath = false;
						continue;
					} else {
						freePath = true;
					}
				} else {
					freePath = false;
				}
			}
			if (neighbor) {
				neighbor.log();
				this.path.push(neighbor);
				tile.isPath = true;
				i++;
			}
			if (minPathLength == i) {
				console.log(finished);
				finished = true;
			}
		}
	}

	generateStartEnd() {
		//0 = left; 1 = top; 2 = right; 3 = bottom
		let side = Random.int(0, 3);
		while (side === this.startSide) {
			side = Random.int(0, 3);
		}
		if (!this.startSide) {
			this.startSide = side;
		}
		const point = {
			x: 0,
			y: 0,
		};
		switch (side) {
			case 0:
				//left [0,0]-[0,n]
				point.x = 0;
				point.y = Random.int(1, this.lengthY - 1);
				break;
			case 1:
				//top [0,0]-[n,10]
				point.x = Random.int(1, this.lengthX - 1);
				point.y = this.lengthX;
				break;
			case 2:
				//right [10,0]-[10,n]
				point.x = this.lengthY;
				point.y = Random.int(1, this.lengthY - 1);
				break;
			case 3:
				//bottom [0,10]-[n,10]
				point.x = Random.int(1, this.lengthY - 1);
				point.y = 0;
				break;
		}
		return this.tiles[point.x][point.y];
	}

	log() {
		for (let i = 0; i <= this.lengthX; i++) {
			let path = '';
			for (let j = 0; j <= this.lengthX; j++) {
				path += this.tiles[i][j].getMarker();
			}
			console.log(`${path}`);
		}
	}
}

map = new Sector(10, 10);
map.log();
