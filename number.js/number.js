function checkType(numbers) {
	if (!Array.isArray(numbers)) {
		throw new Error(`Expected array but got ${typeof numbers}`);
	}
	numbers.forEach((number) => {
		if (typeof number !== 'number') {
			throw new Error(`Expected number but got ${typeof number}`);
		}
	});
}
export function median(numbers) {
	numbers.sort((a, b) => a - b);
	const middleIndex = Math.floor(numbers.length / 2);
	if (numbers.length % 2 === 0) {
		return (numbers[middleIndex - 1] + numbers[middleIndex]) / 2;
	} else {
		return numbers[middleIndex];
	}
}

export function average(numbers) {
	return numbers.reduce((acc, curr) => acc + curr, 0) / numbers.length;
}

export function sum(numbers) {
	return numbers.reduce((acc, curr) => acc + curr, 0);
}

export function lowest(numbers) {
	return Math.min(...numbers);
}

export function highest(numbers) {
	return Math.max(...numbers);
}

export function sortAsc(numbers) {
	return numbers.sort((a, b) => a - b);
}

export function sortDesc(numbers) {
	return numbers.sort((a, b) => b - a);
}

export function roundUp(number, digits) {
	return Math.ceil(number * 10 ** digits) / 10 ** digits;
}

export function roundDown(number, digits) {
	return Math.floor(number * 10 ** digits) / 10 ** digits;
}

export function product(numbers) {
	return numbers.reduce((acc, curr) => acc * curr, 1);
}

export function faculty(number) {
	let result = 1;
	for (let i = 1; i <= number; i++) {
		result *= i;
	}
	return result;
}
