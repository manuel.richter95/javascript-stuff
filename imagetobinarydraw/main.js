getScreamData();

function getImageData() {
    var img = document.getElementById('my-image');
    var canvas = document.createElement('canvas');
    canvas.width = img.width / 10;
    canvas.height = img.height / 10;
    canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height);
    var lines = "";

    for (let i = 1; i <= canvas.height; i++) {
        let line = "";
        for (let j = 1; j <= canvas.width; j++) {
            var pixel = canvas.getContext('2d').getImageData(j, i, 1, 1).data;
            line += getForeGroundColorBasedOnBGBrightness(pixel);
        }
        lines += line + "\n";
    }


    let textarea = document.getElementById("text");
    textarea.value = lines;
}


function getBrightness(pixel) {
    console.log(pixel);
    return Math.sqrt(
        pixel[0] * pixel[0] * .241 +
        pixel[1] * pixel[1] * .691 +
        pixel[2] * pixel[2] * .068);
}

function getForeGroundColorBasedOnBGBrightness(pixel) {
    var brithness = getBrightness(pixel);
    //console.log(brithness);
    if (brithness < 130)
        return 0;
    else
        return 1;
}

function getScreamData() {
    console.log("fsdf");
    document.getElementById("scream").onload = function () {
        console.log("fsdf");
        var c = document.getElementById("myCanvas");
        var ctx = c.getContext("2d");
        var img = document.getElementById("scream");
        console.log("fsdf");
        ctx.drawImage(img, 0, 0);
        var imgData = ctx.getImageData(0, 0, c.width, c.height);
        console.log("fsdf");
        console.log(imgData);
        // invert colors
        var i;
        for (i = 0; i < imgData.data.length; i += 4) {
            imgData.data[i] = 255 - imgData.data[i];
            imgData.data[i + 1] = 255 - imgData.data[i + 1];
            imgData.data[i + 2] = 255 - imgData.data[i + 2];
            imgData.data[i + 3] = 255;
        }
        //ctx.putImageData(imgData, 0, 0);
    };
}