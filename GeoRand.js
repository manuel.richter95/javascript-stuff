
'use strict';

const MAX_LAT = 90;
const MIN_LAT = -90;
const MAX_LNG = 180;
const MIN_LNG = -180;


class GeoRand {

    constructor(bounds) {
        if (!bounds) {
            this.setBounds([[MIN_LAT, MAX_LNG], [MIN_LAT, MIN_LNG]]);
        } else {
            this.setBounds(bounds);
        }
    }

    /**
     * 
     * @param {*} start 
     * @param {*} finish 
     * @param {*} amount 
     * @param {*} options 
     */
    path(start, finish, amount, options) {

    }

    /**
     * 
     * @param {*} bounds 
     */
    setBounds(bounds) {
        if (!Array.isArray(bounds)
            || bounds.length != 2
            || bounds[0].length != 2
            || bounds[1].length != 2
        ) {
            throw new Error("invalid bounds format");
        }
        this.bounds = bounds;
    }

    __getRandomControlPoint() {
        var minLat = this.bounds[0]
    }

    __intersectsBounds(point) {
        var ne = this.bounds[0],
            sw = this.bounds[1],
            sw2, ne2;

        sw2 = ne2 = point;

        return (sw2[0] >= sw[0]) && (ne2[0] <= ne[0]) &&
            (sw2[1] >= sw[1]) && (ne2[1] <= ne[1]);
    }
}

module.export = GeoRand;
