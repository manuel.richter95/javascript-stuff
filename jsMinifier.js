const OPERATORS = ['\u0022', '\u0028', '\u0029', '\u002C', '\u003D', '\u007B', '\u007D', '\u003B', '\u002E','\u003C','\u003E','\u002B'];
const SPACE = '/u0032';
const KEYWORDS = ['let', 'var', 'function', 'return'];

var charCodeIndex = 97;
var charCodeRep = 0;

var Minifier = {
	words: null,
	minifiedWords: null,
	minifiedScript: null,
	minify: script => {
		words = getWords(script);
		minifiedWords = minifyWords(words);
		return replaceWords(words, minifiedWords);
	},
	uglify: script => {
		words = getWords(script);
	}
}

function getWords(script) {
	var words = [];
	var currentWord = "";

	for (var i = 0; i < script.length; i++) {
		var c = script.charAt(i);
		if (isOperator(c)) {
			if (currentWord != '') words.push(currentWord);
			currentWord = "";
			words.push(c);
		} else if (isSpace(c) && currentWord.length > 1) {
			words.push(currentWord);
			currentWord = "";
		} else if (isLetter(c)) {
			currentWord = currentWord.concat(c); //+=c
		}
	}
	return words;
}

function minifyWords(words) {
	console.log(words);
	var variables = [];

	for (let i = 0; i < words.length; i++) {
		var word;
		var nextWord;

		word = words[i];
		nextWord = words[i + 1];
		if (isKeyword(word)) {
			variables.push({
				oldVar: nextWord,
				newVar: manipulateWord(nextWord)
			})
		}
	}
	console.log('variables', variables);
	return variables;
}


function manipulateWord(word) {
	var firstCharCode = 97;
	var lastCharCode = 122;
	if (charCodeIndex > lastCharCode) {
		charCodeIndex = firstCharCode;
		charCodeRep++;
	}
	if (charCodeRep > 0) {
		//
	} else {
		var newChar = String.fromCharCode(charCodeIndex++);
	}

	return newChar;

}

function replaceWords(words, minifiedWords) {
	var newScript = "";

	words.map(word => {
		minifiedWords.map(minifiedWord => {
			if (word === minifiedWord['oldVar']) 
				word = minifiedWord['newVar']			
		})
		newScript = newScript.concat(word); // +=word  
		if (isKeyword(word)) {
			newScript = newScript.concat(" "); // += ""
		}
	})

	return newScript;
}

function isKeyword(word) {
	if (KEYWORDS.find(value => {return word == value;	}))
  	return true;
  else
    return false;
}

function isOperator(c) {
	if (OPERATORS.find(value => {	return c == value;})) 
  	return true;
  else
    return false;
}

function isSpace(c) {
	return (c === SPACE) || (c == " ");
}

function isLetter(c) {
	c = c.charCodeAt(0);
	return (c >= 48 && c <= 57) || (c >= 65 && c <= 90) || (c >= 97 && c <= 122);
}


var script  = "var test = 5 ; function doThis(test){console.log(test);}doThis(5);"
var script2 = "var number = 5; var sum; function calculate(){for(var i = 0;i < number;i++){sum.push(i + number);}}calculate();console.log(sum);" 

function min(){	
	console.log(Minifier.minify(script2));
}
min();

