// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

function getUrl(){
  var queryInfo = {
    active: true,
    currentWindow: true
  };
  chrome.tabs.query(queryInfo, (tabs) => {
    var tab = tabs[0];
    var url = tab.url;
    return url;
  });
}
document.addEventListener('DOMContentLoaded', () => {
  console.log(getUrl());
  var elements = document.getElementsByClassName('yt-simple-endpoint style-scope yt-formatted-string');
  console.log(elements);
  for(var i = 0; i < elements.length; i++){
    var element = elements[i];
    element.style.fontWeight = 600;
  }
  
 
});

chrome.browserAction.onClicked.addListener(function(tab) {
  chrome.tabs.executeScript({
    code: 'document.body.style.backgroundColor="red"'
  });
});